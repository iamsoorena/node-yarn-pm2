FROM ubuntu:16.04

RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash
RUN apt-get install -y nodejs
RUN npm -v
RUN node -v
RUN npm install -g yarn
RUN npm install pm2 -g
WORKDIR /app
ENTRYPOINT [ "/bin/bash" ]